<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'nip', 'position_id', 'department', 'birth_date', 'address', 'phone_number', 'religion', 'status','ktp'
    ];
}
