<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\EmployeeEditRequest;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;




class EmployeeController extends Controller
{

    public function index()
    {
        $employee = Employee::select('*', 'positions.name as position', 'employees.name as fullname', 'employees.id as ID_emp')
            ->join('positions', 'employees.position_id', '=', 'positions.id')
            ->get();
        if (request()->ajax()) {

            return datatables($employee)
                ->addIndexColumn()
                ->addColumn('status', function(Employee $employee){
                if ($employee->status == 1){
                    return '<span class="badge badge-pill badge-success">Aktif</span>';

                }
                else{
                    return '<span class="badge badge-pill badge-danger">Nonaktif</span>';
                }

                })
                ->addColumn('action', function(Employee $employee) {
                    if ($employee->status==1) {
                        $text="Nonaktifkan";
                    }else{
                        $text="Aktifkan";
                    }
                    return
                    '<div class="dropdown">
                    <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Opsi
                    </button>
                    <div class="dropdown-menu" aria-labelledby="actionDropdown">

                      <a class="dropdown-item" href="'.route('employee.show', $employee->ID_emp).'">Detail karyawan</a>
                      <a class="dropdown-item" href="'.route('employee.edit', $employee->ID_emp).'">Edit data karyawan</a>
                      <a class="dropdown-item" href="'.route('switch', $employee->ID_emp).'">'.$text.' karyawan</a>
                      <form action="'.route('employee.destroy', $employee->ID_emp).'" method="post" id="delete_from_'.$employee->ID_emp.'">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="'.csrf_token().'">
                      <button type="submit class="delete_data" class="dropdown-item">Hapus data karyawan</button>

                       </form>
                    </div>
                  </div>
                  ';
                })
                ->rawColumns(['status', 'action'])
                ->toJson();
        }
        return view('employee.index');
    }


    public function create()
    {
        $data = [
            'position'=> Position::all(),
            'currentYear' => Carbon::now()->year,

        ];

        return view('employee.create', $data);
    }


    public function store(EmployeeRequest $request)
    {
        //validate data
        $validatedData = $request->validated();
       // Generate a unique, random name...
        $name=$request->file('ktp')->hashName();
        //upload image
        Storage::putFileAs(
            'public/ktp', $request->file('ktp'), $name
        );

        //store data
        $validatedData['ktp']=$name;
        Employee::create($validatedData);
        Alert::toast('Berhasil menambahkan karyawan', 'success')->autoClose(2000);

        return redirect()->route('employee.index');
    }


    public function show(Employee $employee)
    {
        $data['employee']=Employee::select('*', 'positions.name as position', 'employees.name as fullname', 'employees.id as ID_emp')
        ->join('positions', 'employees.position_id', '=', 'positions.id')
        ->find($employee->id);
        return view('employee.show',$data);
    }


    public function edit(Employee $employee)
    {
        $data = [
            'position'=> Position::all(),
            'currentYear' => Carbon::now()->year,
            'selectedDate'=>Carbon::createFromFormat('Y-m-d', $employee->birth_date)->format('d'),
            'selectedMonth'=>Carbon::createFromFormat('Y-m-d', $employee->birth_date)->format('m'),
            'selectedYear'=>Carbon::createFromFormat('Y-m-d', $employee->birth_date)->format('Y'),
            'employee'=>$employee
        ];
        return view('employee.edit', $data);

    }


    public function update(EmployeeEditRequest $request, Employee $employee)
    {
        $validatedData = $request->validated();
        // if File input has a value

        if ($request->hasFile('ktp')) {
            // Generate a unique, random name...
            $name=$request->file('ktp')->hashName();
            //upload image
            Storage::putFileAs(
                'public/ktp', $request->file('ktp'), $name
            );
            $validatedData['ktp']=$name;
            //delete old file
            $old_path='/ktp/'.$employee->ktp;
            if (Storage::disk('public')->exists($old_path)) {
                Storage::disk('public')->delete($old_path);
            } else {
                return redirect()->back()->withErrors(['error' => 'File not found']);
            }
        }
        Employee::where('id',$employee->id)
        ->update($validatedData);
        Alert::toast('Data '.$employee->name.' berhasil diubah', 'success')->autoClose(2000);

        return redirect()->route('employee.show',$employee->id);

    }

    public function switch($id)
    {
        $employee = Employee::find($id);
        $old_status = $employee->status;
        $alert="";
        $new_status=0;
        //switch status
        if ($old_status==1) {
            $new_status=0;
            $alert="dinonaktifkan";
        }elseif($old_status==0){
            $new_status=1;
            $alert="diaktifkan kembali";
        }
        //update
        $data['status']=$new_status;
        Employee::where('id',$id)
        ->update($data);
        Alert::toast($employee->name.' berhasil '.$alert.'', 'success')->autoClose(2000);
        return redirect()->route('employee.index');
    }
    public function destroy(Employee $employee)
    {
        $path='/ktp/'.$employee->ktp;
        if (Storage::disk('public')->exists($path)) {
            Storage::disk('public')->delete($path);
        } else {
            return redirect()->back()->withErrors(['error' => 'File not found']);
        }
        Employee::destroy($employee->id);
        Alert::toast('Data '.$employee->name.' berhasil dihapus', 'success')->autoClose(2000);
        return redirect()->route('employee.index');

    }
}
