<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|regex:/^[^\d]+$/',
            'nip'=>'required|unique:employees|numeric',
            'position_id'=>'required',
            'department'=>'required',
            'birth_date'=>'required|date',
            'address'=>'required',
            'phone_number'=>'required|numeric',
            'religion'=>'required',
            'status'=>'required',
            'ktp'=>'required|image|mimes:jpeg,png'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Nama lengkap harus diisi',
            'name.regex' => 'Nama tidak boleh mengandung angka',
            'name.string' => 'Nama harus berupa huruf',
            'nip.required'=>'NIP harus diisi',
            'nip.unique'=>'NIP sudah terdaftar',
            'nip.numeric'=>'NIP harus berupa angka',
            'position_id.required'=>'Pilih posisi karyawan',
            'department.required'=>'Departemen harus diisi',
            'birth_date.required'=>'Tanggal lahir harus diisi',
            'birth_date.date'=>'Tanggal lahir harus diisi dengan lengkap',
            'address.required'=>'Alamat harus diisi',
            'phone_number.required'=>'Nomor telepon harus diisi',
            'phone_number.numeric'=>'Nomor telepon harus berupa angka',
            'religion.required'=>'Pilih salah satu agama',
            'ktp.required'=>'Foto KTP harus diunggah',
            'ktp.image'=>'Unggah foto dengan format .png atau .jpg',

        ];
    }

}
