@extends('layout.app')

@section('css')
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail Karyawan </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item"> <a href="{{ route('employee.index') }}">Karyawan</a> </li>
            <li class="breadcrumb-item active" aria-current="page"> Detail Karyawan </li>
        </ol>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 mb-4">
            <a href=" {{ route('employee.edit', $employee->ID_emp) }} " class="btn btn-info mr-2"><i
                    class='fas fa-user-edit mr-2'></i>Edit Data</a>
            <form action="{{ route('employee.destroy', $employee->ID_emp) }}" method="post" class="d-inline">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" class="btn btn-danger mr-2"><i class='fas fa-user-times mr-2'></i>Hapus Data</button>
            </form>
        </div>
        <div class="col-md-6">
            <div class="card sm mb-4">
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between m-auto bg-blue rounded-0 px-5">
                    <h6 class="m-0 font-weight-bold text-primary">Identitas Karyawan </h6>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td> {{ $employee->fullname }} </td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td>:</td>
                            <td> {{ $employee->birth_date }} </td>
                        </tr>
                        <tr>
                            <td>Nomor Telepon</td>
                            <td>:</td>
                            <td> {{ $employee->phone_number }} </td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td> {{ $employee->address }} </td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td>:</td>
                            <td> {{ $employee->religion }} </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="card sm">
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between m-auto bg-blue rounded-0 px-5">
                    <h6 class="m-0 font-weight-bold text-primary">KTP </h6>
                </div>
                <div class="card-body text-center">
                    <img src=" {{ asset('storage/ktp/' . $employee->ktp) }} " class="img-fluid" alt=""
                        srcset="">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card sm">
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between m-auto bg-blue rounded-0 px-5">
                    <h6 class="m-0 font-weight-bold text-primary">Informasi Kepegawaian </h6>
                </div>
                <div class="card-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>NIP</td>
                            <td>:</td>
                            <td> {{ $employee->nip }} </td>
                        </tr>
                        <tr>
                            <td>Status Karyawan</td>
                            <td>:</td>
                            <td>
                                @if ($employee->status == '1')
                                    <h5><span class="badge badge-pill badge-success">Aktif</span></h5>
                                @else
                                    <h5><span class="badge badge-pill badge-danger">Nonaktif</span></h5>
                                @endif
                            </td>
                        </tr>

                        <tr>
                            <td>Posisi</td>
                            <td>:</td>
                            <td> {{ $employee->position }} </td>
                        </tr>
                        <tr>
                            <td>Departemen</td>
                            <td>:</td>
                            <td> {{ $employee->department }} </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
