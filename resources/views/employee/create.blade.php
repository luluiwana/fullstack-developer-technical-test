@extends('layout.app')

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection
@section('breadcrumb')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Data Karyawan</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
            <li class="breadcrumb-item">
                <a href="{{ route('employee.index') }}">Karyawan</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Karyawan</li>
        </ol>
    </div>
@endsection
@section('content')
    <form action="{{ route('employee.store') }}" method="POST" class="row" id="employeeForm"
        enctype="multipart/form-data">
        @csrf
        <div class="col-md-6">
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Identitas Karyawan</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="label" for="name">Nama Lengkap</label>
                        <input type="text" class="form-control" name="name" placeholder="Masukkan nama lengkap"
                            value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <div class="error">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="label d-block" for="birth_date">Tanggal Lahir</label>
                        <select class="custom-select w-auto date_of_birth" id="select_day" name="select_day">
                            <option value="">--</option>
                            @for ($i = 1; $i <= 31; $i++)
                                <option value="{{ $i }}" {{ old('select_day') == $i ? 'selected' : '' }}>
                                    {{ $i }} </option>
                            @endfor
                        </select>
                        <select id="select_month" class="custom-select w-auto date_of_birth" name="select_month">
                            <option value="">--</option>
                            <option value="01" {{ old('select_month') == '01' ? 'selected' : '' }}>Januari</option>
                            <option value="02" {{ old('select_month') == '02' ? 'selected' : '' }}>Februari</option>
                            <option value="03" {{ old('select_month') == '03' ? 'selected' : '' }}>Maret</option>
                            <option value="04" {{ old('select_month') == '04' ? 'selected' : '' }}>April</option>
                            <option value="05" {{ old('select_month') == '05' ? 'selected' : '' }}>Mei</option>
                            <option value="06" {{ old('select_month') == '06' ? 'selected' : '' }}>Juni</option>
                            <option value="07" {{ old('select_month') == '07' ? 'selected' : '' }}>Juli</option>
                            <option value="08" {{ old('select_month') == '08' ? 'selected' : '' }}>Agustus</option>
                            <option value="09" {{ old('select_month') == '09' ? 'selected' : '' }}>September</option>
                            <option value="10" {{ old('select_month') == '10' ? 'selected' : '' }}>Oktober</option>
                            <option value="11" {{ old('select_month') == '11' ? 'selected' : '' }}>November</option>
                            <option value="12" {{ old('select_month') == '12' ? 'selected' : '' }}>Desember</option>
                        </select>

                        <select id="select_year" class="custom-select w-auto date_of_birth" name="select_year">
                            <option value="">--</option>
                            @for ($i = $currentYear; $i > $currentYear - 100; $i--)
                                <option value="{{ $i }}" {{ old('select_year') == $i ? 'selected' : '' }}>
                                    {{ $i }} </option>
                            @endfor
                        </select>
                        <input type="hidden" name="birth_date" id="birth_date">
                        @if ($errors->has('birth_date'))
                            <div class="error">{{ $errors->first('birth_date') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="label" for="phone_number">Nomor Telepon</label>
                        <input type="tel" name="phone_number" class="form-control" placeholder="Masukkan nomor telepon"
                            value="{{ old('phone_number') }}">
                        @if ($errors->has('phone_number'))
                            <div class="error">{{ $errors->first('phone_number') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="label" for="address">Alamat</label>
                        <textarea name="address" class="form-control">{{ old('address') }}</textarea>
                        @if ($errors->has('address'))
                            <div class="error">{{ $errors->first('address') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="label" for="religion">Agama</label>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="religion" value="Islam" id="Islam"
                                {{ old('religion') == 'Islam' ? 'checked' : '' }}>
                            <label class="form-check-label" for="Islam">
                                Islam
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="religion" value="Kristen Protestan"
                                id="Kristen Protestan" {{ old('religion') == 'Kristen Protestan' ? 'checked' : '' }}>
                            <label class="form-check-label" for="Kristen Protestan">
                                Kristen Protestan
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="religion" value="Kristen Katolik"
                                id="Kristen Katolik" {{ old('religion') == 'Kristen Katolik' ? 'checked' : '' }}>
                            <label class="form-check-label" for="Kristen Katolik">
                                Kristen Katolik
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="religion" value="Hindu"
                                id="Hindu" {{ old('religion') == 'Hindu' ? 'checked' : '' }}>
                            <label class="form-check-label" for="Hindu">
                                Hindu
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="religion" value="Buddha"
                                id="Buddha" {{ old('religion') == 'Buddha' ? 'checked' : '' }}>
                            <label class="form-check-label" for="Buddha">
                                Buddha
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="religion" value="Konghucu"
                                id="Konghucu" {{ old('religion') == 'Konghucu' ? 'checked' : '' }}>
                            <label class="form-check-label" for="Konghucu">
                                Konghucu
                            </label>
                        </div>
                        @if ($errors->has('religion'))
                            <div class="error">{{ $errors->first('religion') }}</div>
                        @endif
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Informasi Kepegawaian</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="label d-block" for="status">Status Karyawan</label>
                        <input type="checkbox" checked class="d-block" id="toggleStatus" data-toggle="toggle"
                            data-on="Aktif" data-off="Nonaktif" data-onstyle="success" data-offstyle="danger"
                            data-size="sm">
                        <input type="hidden" name="status" id="status" value="1">


                    </div>
                    <div class="form-group">
                        <label class="label" for="nip">NIP</label>
                        <input type="text" class="form-control" name="nip"
                            placeholder="Masukkan Nomor Induk Pegawai (NIP)" value="{{ old('nip') }}">
                        @if ($errors->has('nip'))
                            <div class="error">{{ $errors->first('nip') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="label" for="position">Posisi</label>
                        <select name="position_id" class="form-control">
                            <option value="">--Pilih Posisi--</option>
                            @foreach ($position as $item)
                                <option value="{{ $item->id }}"
                                    {{ old('position_id') == $item->id ? 'selected' : '' }}> {{ $item->name }} </option>
                            @endforeach
                        </select>
                        @if ($errors->has('position_id'))
                            <div class="error">{{ $errors->first('position_id') }}</div>
                        @endif

                    </div>
                    <div class="form-group">
                        <label class="label" for="department">Departemen</label>
                        <input type="text" class="form-control" name="department"
                            placeholder="Masukkan nama departemen" value="{{ old('department') }}">
                        @if ($errors->has('department'))
                            <div class="error">{{ $errors->first('department') }}</div>
                        @endif
                    </div>

                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Unggah Kartu Tanda Penduduk (KTP)</h6>
                </div>
                <div class="card-body">
                    <div class="custom-file">
                        <input id="logo" type="file" name="ktp" accept=".jpg, .png, .jpeg"
                            class="custom-file-input">
                        <label for="logo" class="custom-file-label text-truncate">Pilih berkas...</label>
                        @if ($errors->has('ktp'))
                            <div class="error">{{ $errors->first('ktp') }}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary ml-3 mt-3 px-5"><i class="fa fa-solid fa-user-plus"></i> Tambah
            Karyawan</button>
    </form>
@endsection
@push('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


    <script>
        $(document).ready(function() {
            changeBirthDate();
            $('.date_of_birth').change(function() {
                changeBirthDate()
            });
            $('.custom-file-input').on('change', function() {
                let fileName = $(this).val().split('\\').pop();
                $(this).next('.custom-file-label').addClass("selected").html(fileName);
            });
            $('#toggleStatus').change(function() {
                if ($('#toggleStatus').is(':checked')) {
                    $('#status').val(1);
                } else {
                    $('#status').val(0);
                }
            });
        });

        function changeBirthDate() {
            var day = $("#select_day option:selected").val();
            var month = $("#select_month option:selected").val();
            var year = $("#select_year option:selected").val();
            if (day < 10) {
                day = '0' + day;
            }
            $('#birth_date').val(year + '-' + month + '-' + day);
        }
    </script>
@endpush
