        document.addEventListener('DOMContentLoaded', function() {
            let thisYear = new Date().getFullYear();
            const yearElem = document.querySelector('#select_year');
            const dayElem = document.querySelector('#select_day');
            const monthElem = document.querySelector('#select_month');

            let days = 31; // maximum number of days
            let months = 12; // maximum number of months
            let yearStart = (yearElem.getAttribute("start")) ? yearElem.getAttribute("start") : thisYear - 100;
            let yearEnd = (yearElem.getAttribute("end")) ? yearElem.getAttribute("end") : thisYear;

            let d = 1;
            while (d <= days) {
                const opt = document.createElement("option");
                opt.setAttribute("value", d);
                opt.innerText = d;
                dayElem.appendChild(opt);
                ++d;
            }
            let m = 1;
            while (m <= months) {
                const opt = document.createElement("option");
                opt.setAttribute("value", m);
                opt.innerText = getMonth(m);
                monthElem.appendChild(opt);
                ++m;
            }
            while (yearStart <= yearEnd) {
                const opt = document.createElement("option");
                opt.setAttribute("value", yearStart);
                opt.innerText = yearStart;
                yearElem.appendChild(opt);
                ++yearStart;
            }
            // yearElem.value = thisYear;

            function getMonth(num) {
                if (num == 1) {
                    return "Januari";
                }
                if (num == 2) {
                    return "Februari";
                }
                if (num == 3) {
                    return "Maret";
                }
                if (num == 4) {
                    return "April";
                }
                if (num == 5) {
                    return "Mei";
                }
                if (num == 6) {
                    return "Juni";
                }
                if (num == 7) {
                    return "Juli";
                }
                if (num == 8) {
                    return "Agustus";
                }
                if (num == 9) {
                    return "September";
                }
                if (num == 10) {
                    return "Oktober";
                }
                if (num == 11) {
                    return "November";
                }
                if (num == 12) {
                    return "Desember";
                }

            }
            //build days
            function buildDays(length) {
                let daysArry = [];
                let d = 1;
                while (d <= 31) {
                    daysArry.push(d);
                    ++d;
                }
                //shorten the length
                daysArry.length = (daysArry.length - length);
                //empty the days element
                dayElem.innerHTML = '';
                //loop through array, then create option tag and append to element
                daysArry.forEach(d => {
                    const opt = document.createElement("option");
                    opt.setAttribute("value", d);
                    opt.innerText = d;
                    dayElem.appendChild(opt);
                });
            }
            //check selected month
            function checkMonth(month) {
                let len = 0;
                //check month that has 30 days
                switch (month) {
                    //september
                    case "9":
                        len = 1;
                        break;
                        //april
                    case "4":
                        len = 1;
                        break;
                        //june
                    case "6":
                        len = 1;
                        break;
                        //november
                    case "11":
                        len = 1;
                        break;
                    default:
                        len = 0;
                        break;
                }
                //check if february is selected
                if (month == 2) {
                    checkLeapYear(yearElem.value);
                } else {
                    buildDays(len);
                }
            }
            //check leap year
            function checkLeapYear(year) {
                let v = (year / 4) * 1;
                //check if the result is an integer or a float using regExp
                if (/^[0-9]+$/.test(v)) {
                    (monthValue == 2) ?
                    buildDays(2) //29 days
                        : '';
                } else {
                    (monthValue == 2) ?
                    buildDays(3) //28 days
                        : '';
                }
            }

            //Month Element Event Listener
            var monthValue = 0
            monthElem.addEventListener('click', function() {
                if (monthValue !== this.value) {
                    monthValue = this.value;
                    checkMonth(this.value); //run the check month function
                }

            });
            //Year Element Event Listener
            var yearValue = 0;
            yearElem.addEventListener('click', function() {
                yearValue = this.value;
                checkLeapYear(this.value); //check for leap year
            });
        })
